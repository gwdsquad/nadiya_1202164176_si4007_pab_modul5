package com.elder.modul5;

import android.os.Bundle;
import android.support.v14.preference.SwitchPreference;
import android.support.v4.app.Fragment;
import android.support.v7.preference.PreferenceFragmentCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class SettingFragment extends PreferenceFragmentCompat {
    SwitchPreference nightMode, bigSize;

    //    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        setPreferencesFromResource(R.xml.setting_pref, rootKey);

//        addPreferencesFromResource(R.xml.setting_pref);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }
    //
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//        nightMode =(SwitchPreference) getPreferenceManager().findPreference("nightMode");
//        bigSize =(SwitchPreference) getPreferenceManager().findPreference("bigSize");
//
//        return inflater.inflate(R.layout.fragment_setting,container,false);
//    }
}

